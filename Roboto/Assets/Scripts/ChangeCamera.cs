using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCamera : MonoBehaviour
{
    public GameObject CameraNewPosition;//aixo es un fill que es posara a la posicio i rotacio de la camera
    public float time;
    public CameraController cameraController;
    void Start()
    {
        cameraController = FindObjectOfType<CameraController>();
        CameraNewPosition = transform.GetChild(0).gameObject;
    }


    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            cameraController.NewCameraParameters(CameraNewPosition.transform, time);
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private bool cameraMoving;
    private Vector3 pos;
    private Quaternion rot;
    private float timeToMove;
    void Start()
    {
        cameraMoving = false;
    }

    
    void Update()
    {
        if (cameraMoving)
        {
            MoveCamera();
        }
    }
    private void MoveCamera()
    {
        transform.position = Vector3.Lerp(transform.position, pos, timeToMove * Time.deltaTime);
        //transform.rotation = Quaternion.RotateTowards(transform.rotation, rot, timeToMove * Time.deltaTime);
        if (Camera.main.transform.position == pos)
        {
            cameraMoving = false;
        }


    }
    public void NewCameraParameters(Transform newPosition, float timeTo)
    {
        pos = newPosition.position;
        rot = newPosition.rotation;
        timeToMove = timeTo;
        cameraMoving = true;
    }
 

}
